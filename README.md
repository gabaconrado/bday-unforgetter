# Birthday Unforgetter

> Project to help you not to forget the beloved ones birthdays!

## Description

Personal project to save birthdays in the cloud and get notifications when
there is a birthday.
Done as a learning project to get familiar with AWS tools and serverless.

## Diagrams

![diagrams](./docs/diagrams.png)
