import json
import os
from typing import Dict

from .configuration import BirthdayAppConfiguration
from .controllers import BirthdayController


def get_controller():
    environment = os.getenv("ENVIRONMENT")
    configuration = BirthdayAppConfiguration(environment)
    use_cases = configuration.get_use_cases()
    return BirthdayController(use_cases())


def handle(event: Dict, context: Dict) -> Dict:
    controller = get_controller()
    method = event["httpMethod"]
    user = event["headers"]["User"]
    data = event.get("body") and json.loads(event["body"])
    (status, body) = controller.handle(user, method, data)
    return {"statusCode": status, "body": json.dumps(body)}
