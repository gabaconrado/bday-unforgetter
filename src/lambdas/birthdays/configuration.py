from commons.protocols import AppConfiguration

from .protocols import UseCases


class BirthdayAppConfiguration(AppConfiguration):

    CLASS_KEY = "BDAY_USE_CASES_CLASS"

    def get_use_cases(self) -> UseCases:
        use_cases_class = self._get_class_from_env()
        return use_cases_class
