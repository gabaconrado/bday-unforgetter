from typing import Dict, Optional, Tuple

from commons import http_commons

from .protocols import BirthdayDTO, Controller, UseCases


class BirthdayController(Controller):
    def __init__(self, _use_cases: UseCases):
        self._use_cases = _use_cases
        self._methods = {
            "POST": (self._add, ["name", "date"]),
            "DELETE": (self._delete, ["pk"]),
            "GET": (self._get, []),
        }

    def handle(
        self, user: str, method: str, body: Optional[Dict] = None
    ) -> Tuple[int, Dict]:
        (method_fn, required_fields) = self._methods.get(method)
        if not http_commons.validate_required_fields(body, required_fields):
            return http_commons.bad_request()
        return method_fn(user, body)

    def _add(self, user: str, body: Dict) -> Dict:
        bday_dto = BirthdayDTO(name=body["name"], user=user, date=body["date"])
        birthday = self._use_cases.add(bday_dto)
        return http_commons.ok(
            status=201, body={"birthday": birthday.to_dict()}
        )

    def _delete(self, user: str, body: Dict) -> Dict:
        self._use_cases.delete(body["pk"])
        return http_commons.ok(200)

    def _get(self, user: str, body: Optional[Dict] = None) -> Dict:
        birthdays = self._use_cases.get(user=user)
        return http_commons.ok(
            status=200,
            body={"birthdays": [birthday.to_dict() for birthday in birthdays]},
        )
