import uuid
from typing import List

from boto3.dynamodb.conditions import Key
from commons.dynamo_db import helpers

from .models import Birthday
from .protocols import BirthdayDTO, UseCases


class DynamoDBUseCases(UseCases):
    def __init__(self):
        self._table = helpers.connect_to_db("Birthdays")
        self._index_name = "userindex"

    def add(self, birthday: BirthdayDTO) -> Birthday:
        pk = str(uuid.uuid4())
        self._table.put_item(
            Item={
                "pk": pk,
                "user": birthday.user,
                "name": birthday.name,
                "date": birthday.date,
            }
        )
        return Birthday(
            pk=pk, user=birthday.user, name=birthday.name, date=birthday.date
        )

    def delete(self, pk: str):
        self._table.delete_item(Key={"pk": pk})

    def get(self, user: str) -> List[Birthday]:
        response = self._table.query(
            IndexName=self._index_name,
            KeyConditionExpression=Key("user").eq(user),
        )
        return [
            Birthday(
                pk=bday["pk"],
                user=bday["user"],
                name=bday["name"],
                date=bday["date"],
            )
            for bday in response["Items"]
        ]
