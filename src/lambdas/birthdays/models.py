class Birthday:
    def __init__(self, pk: str, user: str, name: str, date: str):
        self.pk = pk
        self.user = user
        self.name = name
        self.date = date

    def to_dict(self):
        return {
            "pk": self.pk,
            "user": self.user,
            "name": self.name,
            "date": self.date,
        }
