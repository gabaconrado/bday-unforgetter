from typing import Dict, List, Optional, Tuple

from .models import Birthday


class BirthdayDTO:
    def __init__(
        self, name: str, user: str, date: str, pk: Optional[str] = None
    ):
        self.pk = pk
        self.user = user
        self.name = name
        self.date = date


class UseCases:
    def add(self, birthday: BirthdayDTO) -> Birthday:
        raise NotImplementedError("Cannot call method from interface")

    def delete(self, user: str, pk: str):
        raise NotImplementedError("Cannot call method from interface")

    def get(self, user: str) -> List[Birthday]:
        raise NotImplementedError("Cannot call method from interface")


class Controller:
    def handle(
        self, user: str, method: str, body: Optional[Dict] = None
    ) -> Tuple[int, Dict]:
        raise NotImplementedError("Cannot call method from interface")
