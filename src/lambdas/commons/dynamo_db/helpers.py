import os
from typing import Dict

import boto3


def connect_to_db(table: str) -> object:
    params = _get_parameters()
    db = boto3.resource("dynamodb", **params)
    return db.Table(table)


def _get_parameters() -> Dict:
    params = {
        "region_name": os.getenv("REGION_NAME"),
        "endpoint_url": os.getenv("ENDPOINT_URL"),
    }
    return {k: v for k, v in params.items() if v}
