from typing import Dict, Iterable, Optional, Tuple


def bad_request() -> Tuple[int, Dict]:
    return (400, {"message": "Bad request"})


def ok(status: int, body: Optional[Dict] = {}) -> Tuple[int, Dict]:
    return (status, body)


def validate_required_fields(
    body: Dict, required_fields: Iterable[str]
) -> bool:
    return all([body.get(field) for field in required_fields])


def method_not_allowed() -> Tuple[int, Dict]:
    return (405, {"message": "Method not allowed"})
