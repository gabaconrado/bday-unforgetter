import importlib
import os

import dotenv


class AppConfiguration:

    CLASS_KEY = ""
    CLASSES_KEY = ""

    LAMBDAS_DIR = os.path.dirname(  # /lambdas
        os.path.dirname(  # /lambdas/commons
            os.path.abspath(__file__)  # /lambdas/commons/protocols.py
        )
    )

    def __init__(self, environment: str):
        env_file = os.path.join(self.LAMBDAS_DIR, f".{environment}.env")
        dotenv.load_dotenv(env_file)

    def _get_class_from_env(self) -> object:
        many = self.CLASSES_KEY
        if many:
            return [
                self._get_class_from_str(os.getenv(module)) for module in many
            ]
        class_module = os.getenv(self.CLASS_KEY)
        return self._get_class_from_str(class_module)

    def _get_class_from_str(self, class_module: str) -> object:
        module_index = class_module.rfind(".")
        class_index = module_index + 1
        module_name = class_module[:module_index]
        class_name = class_module[class_index:]
        module = importlib.import_module(module_name)
        return getattr(module, class_name)
