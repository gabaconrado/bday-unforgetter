import json
import os
from typing import Dict

from .configuration import NotifierAppConfiguration
from .controllers import NotifierController


def get_controller():
    environment = os.getenv("ENVIRONMENT")
    configuration = NotifierAppConfiguration(environment)
    use_cases = [uc() for uc in configuration.get_use_cases()]
    return NotifierController(*use_cases)


def handle(event: Dict, context: Dict) -> Dict[int, Dict]:
    controller = get_controller()
    (status, body) = controller.handle()
    return {"statusCode": status, "body": json.dumps(body)}
