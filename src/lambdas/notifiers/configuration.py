from commons.protocols import AppConfiguration

from .protocols import UseCases


class NotifierAppConfiguration(AppConfiguration):

    CLASSES_KEY = [
        "USER_USE_CASES_CLASS",
        "BDAY_USE_CASES_CLASS",
        "NOTIFIER_USE_CASES_CLASS",
    ]

    def get_use_cases(self) -> UseCases:
        use_cases_classes = self._get_class_from_env()
        return use_cases_classes
