from typing import Dict, List, Tuple

from birthdays.protocols import UseCases as BdayUseCases
from users.protocols import UseCases as UserUseCases

from . import utils
from .models import Birthday, User
from .protocols import Controller, UseCases


class NotifierController(Controller):
    def __init__(
        self,
        _user_use_cases: UserUseCases,
        _bday_use_cases: BdayUseCases,
        _notifier_use_cases: UseCases,
    ):
        self._bday_use_cases = _bday_use_cases
        self._user_use_cases = _user_use_cases
        self._notifier_use_cases = _notifier_use_cases
        self._methods = {"POST": self._notify}

    def handle(self) -> Tuple[int, Dict]:
        return self._notify()

    def _notify(self):
        all_users = self._fetch_data()
        self._notifier_use_cases.notify(all_users)
        return (200, {})

    def _fetch_data(self) -> List[User]:
        users = [
            User(
                name=user.name,
                email=user.email,
                birthdays=[
                    Birthday(bday.name, utils.calculate_age(bday.date))
                    for bday in self._bday_use_cases.get(user.pk)
                    if utils.is_birthday_today(bday.date)
                ],
            )
            for user in self._user_use_cases.get()
        ]
        return [user for user in users if user.birthdays]
