from typing import List


class Birthday:
    def __init__(self, name: str, age: int):
        self.name = name
        self.age = age


class User:
    def __init__(self, name: str, email: str, birthdays: List[Birthday]):
        self.name = name
        self.email = email
        self.birthdays = birthdays
