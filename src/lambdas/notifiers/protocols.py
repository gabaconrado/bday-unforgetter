from typing import Dict, List, Tuple

from .models import User


class UseCases:
    def notify(self, users: List[User]):
        raise NotImplementedError("Cannot call method from interface")


class Controller:
    def handle(self) -> Tuple[int, Dict]:
        raise NotImplementedError("Cannot call method from interface")
