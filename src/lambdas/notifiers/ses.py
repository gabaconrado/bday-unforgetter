import os
from typing import List

import boto3

from .models import User
from .protocols import UseCases


class SESUseCase(UseCases):

    sender = f"Birthday Unforgetter <{os.getenv('MAIL_SENDER')}>"
    subject = "Today's birthdays!"
    charset = "UTF-8"

    def __init__(self):
        self._client = boto3.client(
            "ses", region_name=os.getenv("REGION_NAME")
        )

    def notify(self, users: List[User]):
        for user in users:
            self._send_mail(user)

    def _send_mail(self, user: User):
        (text_content, html_content) = self._build_content(user)
        self._client.send_email(
            Destination={
                "ToAddresses": [user.email],
            },
            Source=self.sender,
            Message={
                "Body": {
                    "Html": {
                        "Charset": self.charset,
                        "Data": html_content,
                    },
                    "Text": {
                        "Charset": self.charset,
                        "Data": text_content,
                    },
                },
                "Subject": {
                    "Charset": self.charset,
                    "Data": self.subject,
                },
            },
        )

    def _build_content(self, user: User):
        birthdays = ", ".join(
            [f"{bday.name}({bday.age})" for bday in user.birthdays]
        )
        text_content = (
            f"Hello {user.name}, here is a list "
            f"of today's birthdays: {birthdays}"
        )
        html_content = f"<html><head></head><body>{text_content}</body></html>"
        return (text_content, html_content)
