from datetime import date


def is_birthday_today(birthday: str):
    bday = date.fromisoformat(birthday)
    today = date.today()
    return bday.day == today.day and bday.month == today.month


def calculate_age(birthday: str):
    bday = date.fromisoformat(birthday)
    today = date.today()
    return int((today - bday).days / 365.2325)
