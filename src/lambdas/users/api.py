import json
import os

from .configuration import UserAppConfiguration
from .controllers import UserController


def get_controller():
    environment = os.getenv("ENVIRONMENT")
    configuration = UserAppConfiguration(environment)
    use_cases = configuration.get_use_cases()
    return UserController(use_cases())


def handle(event: dict, context: object) -> dict:
    controller = get_controller()
    method = event["httpMethod"]
    data = event.get("body") and json.loads(event["body"])
    (status, body) = controller.handle(method, data)
    return {"statusCode": status, "body": json.dumps(body)}
