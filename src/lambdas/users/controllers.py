from typing import Dict, List, Optional, Tuple

from commons import http_commons

from .models import User
from .protocols import Controller, UseCases, UserDTO


class UserController(Controller):
    def __init__(self, _user_use_cases: UseCases):
        self._user_use_cases = _user_use_cases
        self._methods = {
            "POST": (self._add, ["name", "email"]),
            "GET": (self._get, []),
            "PUT": (self._edit, ["pk", "name", "email"]),
            "DELETE": (self._delete, ["pk"]),
        }

    def handle(
        self, method: str, body: Optional[Dict] = None
    ) -> Tuple[int, Dict]:
        (method_fn, required_fields) = self._methods.get(method, (None, None))
        if not method_fn:
            return http_commons.method_not_allowed()
        if not http_commons.validate_required_fields(body, required_fields):
            return http_commons.bad_request()
        args = (body,) if body else ()
        return method_fn(*args)

    def _add(self, body: Dict) -> User:
        user_dto = UserDTO(name=body["name"], email=body["email"])
        user = self._user_use_cases.add(user_dto)
        return http_commons.ok(status=201, body={"user": user.to_dict()})

    def _edit(self, body: Dict):
        user_dto = UserDTO(
            pk=body["pk"], name=body["name"], email=body["email"]
        )
        self._user_use_cases.edit(user_dto)
        return http_commons.ok(status=200)

    def _get(self) -> List[User]:
        users = self._user_use_cases.get()
        return http_commons.ok(
            status=200, body={"users": [user.to_dict() for user in users]}
        )

    def _delete(self, body: Dict):
        self._user_use_cases.delete(body["pk"])
        return http_commons.ok(status=200)
