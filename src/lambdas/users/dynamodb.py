import uuid
from typing import List

from commons.dynamo_db import helpers

from .models import User
from .protocols import UseCases, UserDTO


class DynamoDBUseCases(UseCases):
    def __init__(self):
        self._table = helpers.connect_to_db("Users")

    def add(self, user: UserDTO) -> User:
        pk = str(uuid.uuid4())
        self._table.put_item(
            Item={
                "pk": pk,
                "name": user.name,
                "email": user.email,
            }
        )
        return User(pk=pk, name=user.name, email=user.email)

    def edit(self, user: UserDTO):
        self._table.update_item(
            Key={"pk": user.pk},
            UpdateExpression="set #name=:n, #email=:e",
            ExpressionAttributeValues={
                ":n": user.name,
                ":e": user.email,
            },
            ExpressionAttributeNames={"#name": "name", "#email": "email"},
        )

    def get(self) -> List[User]:
        return [
            User(pk=user["pk"], name=user["name"], email=user["email"])
            for user in self._table.scan()["Items"]
        ]

    def delete(self, pk: str):
        self._table.delete_item(Key={"pk": pk})
