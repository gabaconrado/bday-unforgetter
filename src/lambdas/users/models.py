class User:
    def __init__(self, pk: str, name: str, email: str):
        self.pk = pk
        self.name = name
        self.email = email

    def to_dict(self):
        return {
            "pk": self.pk,
            "name": self.name,
            "email": self.email,
        }
