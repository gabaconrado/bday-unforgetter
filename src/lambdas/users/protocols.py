from typing import Dict, List, Optional, Tuple

from .models import User


class UserDTO:
    def __init__(self, name: str, email: str, pk: Optional[str] = None):
        self.pk = pk
        self.name = name
        self.email = email


class UseCases:
    def add(self, user: UserDTO) -> User:
        raise NotImplementedError("Cannot call method from interface")

    def edit(self, user: UserDTO):
        raise NotImplementedError("Cannot call method from interface")

    def get(self) -> List[User]:
        raise NotImplementedError("Cannot call method from interface")

    def delete(self, pk: str):
        raise NotImplementedError("Cannot call method from interface")


class Controller:
    def handle(
        self, method: str, body: Optional[Dict] = None
    ) -> Tuple[int, Dict]:
        raise NotImplementedError("Cannot call method from interface")
