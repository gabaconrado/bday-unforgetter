#!/bin/bash

docker-compose up -d
chown -R $USER dynamodb
sam build
sam local start-api --docker-network lambda-local --env-vars .development.json
docker-compose down
