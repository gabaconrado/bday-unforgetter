import os

import boto3


def _connect_to_db():
    os.environ["AWS_ACCESS_KEY_ID"] = "123"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "123"
    params = {
        "endpoint_url": "http://localhost:8000",
        "region_name": "us-east-1",
    }
    return boto3.resource("dynamodb", **params)


def create_user_table(db):
    db.create_table(
        TableName="Users",
        KeySchema=[
            {
                "AttributeName": "pk",
                "KeyType": "HASH",
            },
        ],
        AttributeDefinitions=[
            {
                "AttributeName": "pk",
                "AttributeType": "S",
            },
        ],
        ProvisionedThroughput={
            "ReadCapacityUnits": 10,
            "WriteCapacityUnits": 10,
        },
    )


def create_birthday_table(db):
    db.create_table(
        TableName="Birthdays",
        KeySchema=[
            {
                "AttributeName": "pk",
                "KeyType": "HASH",
            },
        ],
        AttributeDefinitions=[
            {
                "AttributeName": "pk",
                "AttributeType": "S",
            },
            {
                "AttributeName": "user",
                "AttributeType": "S",
            },
        ],
        GlobalSecondaryIndexes=[
            {
                "IndexName": "userindex",
                "KeySchema": [
                    {
                        "AttributeName": "user",
                        "KeyType": "HASH",
                    },
                ],
                "Projection": {
                    "ProjectionType": "ALL",
                },
                "ProvisionedThroughput": {
                    "ReadCapacityUnits": 1,
                    "WriteCapacityUnits": 1,
                },
            },
        ],
        ProvisionedThroughput={
            "ReadCapacityUnits": 10,
            "WriteCapacityUnits": 10,
        },
    )


def main():
    db = _connect_to_db()
    create_user_table(db)
    create_birthday_table(db)


if __name__ == "__main__":
    main()
