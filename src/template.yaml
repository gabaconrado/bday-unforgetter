AWSTemplateFormatVersion: '2010-09-09'
Transform: AWS::Serverless-2016-10-31
Description: >
  Resources specifications for the Birthday Unforgetter project

Globals:
  Function:
    Timeout: 15

Resources:
  UserManagementFunction:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: lambdas/
      Environment: {
        "Variables": {
          "ENVIRONMENT": "production"
        }
      }
      Handler: users.api.handle
      Runtime: python3.8
      Policies:
        DynamoDBCrudPolicy:
          TableName: !Ref UserTable
      Events:
        HttpUserManagementEndPoint:
          Type: Api
          Properties:
            Path: /users
            Method: ANY
            Auth:
              ApiKeyRequired: true
              UsagePlan:
                CreateUsagePlan: PER_API
                UsagePlanName: GatewayAuthorization

  BirthdayManagementFunction:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: lambdas/
      Environment: {
        "Variables": {
          "ENVIRONMENT": "production"
        }
      }
      Handler: birthdays.api.handle
      Runtime: python3.8
      Policies:
        DynamoDBCrudPolicy:
          TableName: !Ref BirthdayTable
      Events:
        HttpBirthdayManagementEndPoint:
          Type: Api
          Properties:
            Path: /birthdays
            Method: ANY
            Auth:
              ApiKeyRequired: true
              UsagePlan:
                CreateUsagePlan: PER_API
                UsagePlanName: GatewayAuthorization
            RequestParameters:
              - method.request.header.User:
                  Required: true
                  Caching: false

  BirthdayNotificationFunction:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: lambdas/
      Environment: {
        "Variables": {
          "ENVIRONMENT": "production"
        }
      }
      Handler: notifiers.api.handle
      Runtime: python3.8
      Policies:
        - DynamoDBCrudPolicy:
            TableName: !Ref UserTable
        - DynamoDBCrudPolicy:
            TableName: !Ref BirthdayTable
        - SESCrudPolicy:
            IdentityName: gabaconrado@gmail.com
      Events:
        HttpBirthdayNotificationEndPoint:
          Type: Api
          Properties:
            Path: /birthdays/notify
            Method: POST
            Auth:
              ApiKeyRequired: true
              UsagePlan:
                CreateUsagePlan: PER_API
                UsagePlanName: GatewayAuthorization
        BdayDailyNotification:
          Type: Schedule
          Properties:
            Schedule: cron(0 10 * * ? *)

  UserTable:
    Type: AWS::DynamoDB::Table
    Properties:
      TableName: Users
      AttributeDefinitions:
        - AttributeName: pk
          AttributeType: S
      KeySchema:
        - AttributeName: pk
          KeyType: HASH
      ProvisionedThroughput:
        ReadCapacityUnits: 5
        WriteCapacityUnits: 5

  BirthdayTable:
    Type: AWS::DynamoDB::Table
    Properties:
      TableName: Birthdays
      AttributeDefinitions:
        - AttributeName: pk
          AttributeType: S
        - AttributeName: user
          AttributeType: S
      KeySchema:
        - AttributeName: pk
          KeyType: HASH
      GlobalSecondaryIndexes:
        - IndexName: userindex
          KeySchema:
            - AttributeName: user
              KeyType: HASH
          Projection:
            ProjectionType: ALL
          ProvisionedThroughput:
            ReadCapacityUnits: 1
            WriteCapacityUnits: 1
      ProvisionedThroughput:
        ReadCapacityUnits: 5
        WriteCapacityUnits: 5

Outputs:
  UserManagementFunction:
    Description: "User management Lambda Function ARN"
    Value: !GetAtt UserManagementFunction.Arn
  UserApi:
    Description: "Generated UserManagementFunction Api."
    Value: !Sub "https://${ServerlessRestApi}.execute-api.${AWS::Region}.amazonaws.com/Prod/users"
  BirthdayApi:
    Description: "Generated BirthdayManagementFunction Api."
    Value: !Sub "https://${ServerlessRestApi}.execute-api.${AWS::Region}.amazonaws.com/Prod/birthdays"
  ApiKey:
    Description: "You can find your API Key in the AWS console: (Put in the request HEADER as 'x-api-key')"
    Value: !Sub "https://console.aws.amazon.com/apigateway/home?region=${AWS::Region}#/api-keys/"
