import json

import pytest
from birthdays.api import handle


class TestBirthdayApi:
    @pytest.fixture
    def context(self):
        return {}

    @pytest.fixture
    def event(self):
        return {
            "httpMethod": "POST",
            "headers": {"User": "1"},
            "body": json.dumps({"data": "data"}),
        }

    @pytest.fixture
    def controller_mock(self, mocker):
        return mocker.patch(
            "birthdays.api.BirthdayController.handle",
            return_value=(200, {}),
        )

    def test_handle(self, context, event, controller_mock):
        response = handle(event, context)
        controller_mock.assert_called_once_with(
            "1",
            "POST",
            {"data": "data"},
        )
        assert response == {"statusCode": 200, "body": "{}"}
