import os

import pytest
from birthdays.configuration import BirthdayAppConfiguration
from birthdays.protocols import UseCases


class TestBirthdayAppConfiguration:
    @pytest.fixture
    def configuration(self):
        return BirthdayAppConfiguration("test")

    def test_initialization(self, configuration):
        assert configuration
        assert (
            os.getenv("BDAY_USE_CASES_CLASS") == "birthdays.protocols.UseCases"
        )

    def test_get_use_cases(self, configuration):
        use_cases_class = configuration.get_use_cases()
        assert use_cases_class == UseCases
