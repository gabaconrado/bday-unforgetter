import pytest
from birthdays.controllers import BirthdayController
from birthdays.models import Birthday
from birthdays.protocols import UseCases


class TestBirthdayController:

    SAMPLE_USER_PK = "1"
    SAMPLE_BDAY_PK = "2"

    @pytest.fixture
    def user_dict(self):
        return {"pk": "1", "name": "Gaba", "email": "gaba@gaba.com"}

    @pytest.fixture
    def add_birthday_request(self, user_dict):
        return (
            self.SAMPLE_USER_PK,
            "POST",
            {
                "user": user_dict["pk"],
                "name": "Ronaldinho Gaúcho",
                "date": "1980-03-21",
            },
        )

    @pytest.fixture(params=["name", "date"])
    def add_birthday_request_wrong(self, add_birthday_request, request):
        birthday = add_birthday_request[-1]
        birthday.pop(request.param)
        return add_birthday_request

    @pytest.fixture
    def delete_birthday_request(self, user_dict):
        return (
            self.SAMPLE_USER_PK,
            "DELETE",
            {"user": user_dict["pk"], "pk": self.SAMPLE_BDAY_PK},
        )

    @pytest.fixture(params=["pk"])
    def delete_birthday_request_wrong(self, delete_birthday_request, request):
        body = delete_birthday_request[-1]
        body.pop(request.param)
        return delete_birthday_request

    @pytest.fixture
    def get_birthday_request(self, user_dict):
        return (
            self.SAMPLE_USER_PK,
            "GET",
            {"user": user_dict["pk"]},
        )

    @pytest.fixture
    def birthday(self, add_birthday_request):
        birthday = add_birthday_request[-1]
        return Birthday(
            pk=self.SAMPLE_BDAY_PK,
            user=birthday["user"],
            name=birthday["name"],
            date=birthday["date"],
        )

    @pytest.fixture
    def use_cases_mock(self, mocker, birthday):
        obj = UseCases()
        mocker.patch.object(obj, "add", return_value=birthday, auto_spec=True)
        mocker.patch.object(obj, "delete", auto_spec=True)
        mocker.patch.object(
            obj, "get", return_value=[birthday], auto_spec=True
        )
        return obj

    @pytest.fixture
    def controller(self, use_cases_mock):
        return BirthdayController(use_cases_mock)

    def test_birthday_controller_init(self):
        usecases = UseCases()
        controller = BirthdayController(usecases)
        assert controller
        assert controller._use_cases == usecases

    def test_add(self, use_cases_mock, add_birthday_request, controller):
        (user, method, body) = add_birthday_request
        result = controller.handle(user, method, body)
        use_cases_mock.add.assert_called_once()
        assert result == (
            201,
            {
                "birthday": {
                    "pk": self.SAMPLE_BDAY_PK,
                    "user": body["user"],
                    "name": body["name"],
                    "date": body["date"],
                }
            },
        )

    def test_add_invalid_request(
        self, use_cases_mock, add_birthday_request_wrong, controller
    ):
        (user, method, body) = add_birthday_request_wrong
        result = controller.handle(user, method, body)
        use_cases_mock.add.assert_not_called()
        assert result == (
            400,
            {"message": "Bad request"},
        )

    def test_delete(self, use_cases_mock, delete_birthday_request, controller):
        (user, method, body) = delete_birthday_request
        result = controller.handle(user, method, body)
        use_cases_mock.delete.assert_called_once_with(body["pk"])
        assert result == (200, {})

    def test_delete_invalid_request(
        self, use_cases_mock, delete_birthday_request_wrong, controller
    ):
        (user, method, body) = delete_birthday_request_wrong
        result = controller.handle(user, method, body)
        use_cases_mock.delete.assert_not_called()
        assert result == (
            400,
            {"message": "Bad request"},
        )

    def test_get(
        self, birthday, use_cases_mock, get_birthday_request, controller
    ):
        (user, method, body) = get_birthday_request
        result = controller.handle(user, method, body)
        use_cases_mock.get.assert_called_once_with(user=body["user"])
        assert result == (200, {"birthdays": [birthday.to_dict()]})
