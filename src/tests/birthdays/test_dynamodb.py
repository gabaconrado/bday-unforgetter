import pytest
from birthdays.dynamodb import DynamoDBUseCases
from birthdays.models import Birthday
from birthdays.protocols import BirthdayDTO


class TestDynamoDBBirthdayUseCases:

    SAMPLE_USER_PK = "1"
    SAMPLE_BDAY_PK = "2"

    @pytest.fixture(autouse=True)
    def dynamo_mock(self, mocker):
        return mocker.patch("boto3.resource")

    @pytest.fixture
    def dynamo_uc(self):
        return DynamoDBUseCases()

    @pytest.fixture
    def dynamo_table_mock(self, dynamo_uc, birthday_dict, birthday):
        mock = dynamo_uc._table
        mock.add.return_value = birthday
        mock.query.return_value = {"Items": [birthday_dict]}
        return dynamo_uc._table

    @pytest.fixture
    def birthday_dto(self):
        return BirthdayDTO(
            pk=self.SAMPLE_BDAY_PK,
            name="Ronaldinho Gaúcho",
            user=self.SAMPLE_USER_PK,
            date="1980-03-21",
        )

    @pytest.fixture
    def birthday(self, birthday_dto):
        return Birthday(
            pk=birthday_dto.pk,
            user=birthday_dto.user,
            name=birthday_dto.name,
            date=birthday_dto.date,
        )

    @pytest.fixture
    def birthday_dict(self, birthday):
        return birthday.to_dict()

    def test_initialization(self, dynamo_uc):
        assert dynamo_uc

    def test_add(self, dynamo_uc, dynamo_table_mock, birthday_dto):
        birthday = dynamo_uc.add(birthday_dto)
        dynamo_table_mock.put_item.assert_called_once_with(
            Item={
                "pk": birthday.pk,
                "user": birthday.user,
                "name": birthday.name,
                "date": birthday.date,
            }
        )
        assert all(
            [
                birthday.user == birthday_dto.user,
                birthday.name == birthday_dto.name,
                birthday.date == birthday_dto.date,
            ]
        )

    def test_get(self, dynamo_uc, dynamo_table_mock, birthday_dict):
        birthdays = dynamo_uc.get(birthday_dict["user"])
        dynamo_table_mock.query.assert_called_once()
        birthday = birthdays[0]
        assert all(
            [
                birthday.pk == birthday_dict["pk"],
                birthday.user == birthday_dict["user"],
                birthday.name == birthday_dict["name"],
                birthday.date == birthday_dict["date"],
            ]
        )

    def test_delete(self, dynamo_uc, dynamo_table_mock):
        dynamo_uc.delete(self.SAMPLE_BDAY_PK)
        dynamo_table_mock.delete_item.assert_called_once_with(
            Key={"pk": self.SAMPLE_BDAY_PK},
        )
