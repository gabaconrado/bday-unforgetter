import pytest
from birthdays.models import Birthday


class TestModels:

    SAMPLE_PK = "1"
    SAMPLE_USER = "2"

    @pytest.fixture
    def birthday(self):
        return Birthday(
            pk=self.SAMPLE_PK,
            user=self.SAMPLE_USER,
            name="Ronaldinho Gaúcho",
            date="1980-03-21",
        )

    def test_to_dict(self, birthday):
        result = birthday.to_dict()
        assert result == {
            "pk": birthday.pk,
            "user": birthday.user,
            "name": birthday.name,
            "date": birthday.date,
        }
