import sys
from os.path import abspath, dirname, join

src_dir = dirname(dirname(abspath(__file__)))
sys.path.append(join(src_dir, "lambdas"))
