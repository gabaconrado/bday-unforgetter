import pytest
from notifiers.api import handle


class TestNotifierApi:
    @pytest.fixture
    def context(self):
        return {}

    @pytest.fixture
    def event(self):
        return {}

    @pytest.fixture
    def controller_mock(self, mocker):
        return mocker.patch(
            "notifiers.api.NotifierController.handle",
            return_value=(200, {}),
        )

    def test_handle(self, context, event, controller_mock):
        response = handle(event, context)
        controller_mock.assert_called_once_with()
        assert response == {"statusCode": 200, "body": "{}"}
