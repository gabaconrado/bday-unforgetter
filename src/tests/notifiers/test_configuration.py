import os

import pytest
from birthdays.protocols import UseCases as BdayUseCases
from notifiers.configuration import NotifierAppConfiguration
from notifiers.protocols import UseCases
from users.protocols import UseCases as UserUseCases


class TestUserAppConfiguration:
    @pytest.fixture
    def configuration(self):
        return NotifierAppConfiguration("test")

    def test_initialization(self, configuration):
        assert configuration
        assert os.getenv("USER_USE_CASES_CLASS") == "users.protocols.UseCases"
        assert (
            os.getenv("BDAY_USE_CASES_CLASS") == "birthdays.protocols.UseCases"
        )
        assert (
            os.getenv("NOTIFIER_USE_CASES_CLASS")
            == "notifiers.protocols.UseCases"
        )

    def test_get_use_cases(self, configuration):
        (user_uc, bday_uc, notifier_uc) = configuration.get_use_cases()
        assert (user_uc, bday_uc, notifier_uc) == (
            UserUseCases,
            BdayUseCases,
            UseCases,
        )
