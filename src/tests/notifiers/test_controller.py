import pytest
from birthdays.models import Birthday
from birthdays.protocols import UseCases as BdayUseCases
from notifiers.controllers import NotifierController
from notifiers.protocols import UseCases
from users.models import User
from users.protocols import UseCases as UserUseCases


class TestNotifierController:
    @pytest.fixture
    def controller(self, use_cases_mock):
        return NotifierController(*use_cases_mock)

    @pytest.fixture
    def use_cases_mock(self, mocker, birthday, user):
        bday_uc = BdayUseCases()
        user_uc = UserUseCases()
        notify_uc = UseCases()
        mocker.patch.object(bday_uc, "get", return_value=[birthday])
        mocker.patch.object(user_uc, "get", return_value=[user])
        mocker.patch.object(notify_uc, "notify", autospec=True)
        return (user_uc, bday_uc, notify_uc)

    @pytest.fixture
    def birthday(self):
        return Birthday(pk="1", user="1", name="Gaba", date="1993-04-10")

    @pytest.fixture
    def user(self):
        return User(pk="1", name="Gaba", email="gaba@gaba.com")

    def test_initialization(self, controller, use_cases_mock):
        assert (
            controller._user_use_cases,
            controller._bday_use_cases,
            controller._notifier_use_cases,
        ) == use_cases_mock

    def test_handle(self, controller, use_cases_mock):
        (user_uc, bday_uc, notify_uc) = use_cases_mock
        response = controller.handle()
        notify_uc.notify.assert_called_once()
        assert response == (200, {})
