import pytest
from notifiers.models import Birthday, User
from notifiers.ses import SESUseCase


class TestSESUseCase:
    @pytest.fixture(autouse=True)
    def ses_mock(self, mocker):
        return mocker.patch("boto3.client")

    @pytest.fixture
    def use_case(self):
        return SESUseCase()

    @pytest.fixture
    def users(self):
        return [
            User(
                name="Gaba",
                email="gabaconrado@gmail.com",
                birthdays=[Birthday(name="Gaba", age=28)],
            ),
        ]

    @pytest.fixture
    def mail_content_html(self, mail_content_text):
        return (
            "<html><head></head><body>" f"{mail_content_text}" "</body></html>"
        )

    @pytest.fixture
    def mail_content_text(self):
        return "Hello Gaba, here is a list of today's birthdays: Gaba(28)"

    def test_initialization(self, use_case, ses_mock):
        assert use_case._client
        ses_mock.assert_called_once()

    def test_notify(
        self, use_case, users, mail_content_html, mail_content_text
    ):
        use_case.notify(users)
        use_case._client.send_email.assert_called_once_with(
            Destination={
                "ToAddresses": ["gabaconrado@gmail.com"],
            },
            Source="Birthday Unforgetter <test@test.com>",
            Message={
                "Body": {
                    "Html": {
                        "Charset": "UTF-8",
                        "Data": mail_content_html,
                    },
                    "Text": {
                        "Charset": "UTF-8",
                        "Data": mail_content_text,
                    },
                },
                "Subject": {
                    "Charset": "UTF-8",
                    "Data": "Today's birthdays!",
                },
            },
        )
