import pytest
from notifiers import utils

DATE = "1990-01-01"


class TestUtils:
    @pytest.mark.freeze_time(DATE)
    @pytest.mark.parametrize(
        "input_date, expected",
        [
            ("2021-01-01", True),
            ("2021-01-02", False),
            ("2021-02-01", False),
            ("2021-02-02", False),
        ],
    )
    def test_is_birthday_today(self, input_date, expected):
        assert utils.is_birthday_today(input_date) == expected

    @pytest.mark.freeze_time(DATE)
    @pytest.mark.parametrize(
        "input_date, expected",
        [
            ("1970-01-01", 20),
            ("1969-12-31", 20),
            ("1970-01-02", 19),
        ],
    )
    def test_calculate_age(self, input_date, expected):
        assert utils.calculate_age(input_date) == expected
