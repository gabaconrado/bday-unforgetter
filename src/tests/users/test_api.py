import json

import pytest
from users.api import handle


class TestUserApi:
    @pytest.fixture
    def context(self):
        return {}

    @pytest.fixture
    def event(self):
        return {"httpMethod": "POST", "body": json.dumps({"data": "data"})}

    @pytest.fixture
    def controller_mock(self, mocker):
        return mocker.patch(
            "users.api.UserController.handle",
            return_value=(200, {}),
        )

    def test_handle(self, context, event, controller_mock):
        response = handle(event, context)
        controller_mock.assert_called_once_with(
            "POST",
            {"data": "data"},
        )
        assert response == {"statusCode": 200, "body": "{}"}
