import os

import pytest
from users.configuration import UserAppConfiguration
from users.protocols import UseCases


class TestUserAppConfiguration:
    @pytest.fixture
    def configuration(self):
        return UserAppConfiguration("test")

    def test_initialization(self, configuration):
        assert configuration
        assert os.getenv("USER_USE_CASES_CLASS") == "users.protocols.UseCases"

    def test_get_use_cases(self, configuration):
        use_cases_class = configuration.get_use_cases()
        assert use_cases_class == UseCases
