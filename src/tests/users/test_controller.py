import pytest
from users.controllers import UserController
from users.models import User
from users.protocols import UseCases


class TestUserController:

    SAMPLE_PK = 1

    @pytest.fixture
    def user_dict(self):
        return {
            "name": "Gaba",
            "email": "gaba@gaba.com",
        }

    @pytest.fixture
    def user_with_pk_dict(self):
        return {
            "pk": self.SAMPLE_PK,
            "name": "Gaba2",
            "email": "gaba2@gaba.com",
        }

    @pytest.fixture
    def user_pk_dict(self):
        return {"pk": self.SAMPLE_PK}

    @pytest.fixture
    def delete_request(self, user_pk_dict):
        return ("DELETE", user_pk_dict)

    @pytest.fixture
    def delete_request_wrong(self, delete_request):
        pk_dict = delete_request[1]
        del pk_dict["pk"]
        return delete_request

    @pytest.fixture
    def put_request(self, user_with_pk_dict):
        return ("PUT", user_with_pk_dict)

    @pytest.fixture(params=["pk", "name", "email"])
    def put_request_wrong(self, put_request, request):
        user_dict = put_request[1]
        del user_dict[request.param]
        return put_request

    @pytest.fixture
    def get_request(self):
        return ("GET", None)

    @pytest.fixture
    def post_request(self, user_dict):
        return ("POST", user_dict)

    @pytest.fixture(params=["name", "email"])
    def post_request_wrong(self, post_request, request):
        user_dict = post_request[1]
        del user_dict[request.param]
        return post_request

    @pytest.fixture
    def invalid_method_request(self):
        return ("PATCH", None)

    @pytest.fixture
    def user(self):
        return User(
            pk=self.SAMPLE_PK,
            name="Gaba",
            email="gaba@gaba.com",
        )

    @pytest.fixture
    def users(self, user):
        return [user]

    @pytest.fixture(autouse=True)
    def use_cases_mock(self, mocker, user, users):
        obj = UseCases()
        mocker.patch.object(obj, "add", return_value=user, auto_spec=True)
        mocker.patch.object(obj, "delete", auto_spec=True)
        mocker.patch.object(obj, "edit", auto_spec=True)
        mocker.patch.object(obj, "get", return_value=users, auto_spec=True)
        return obj

    @pytest.fixture
    def controller(self, use_cases_mock):
        return UserController(use_cases_mock)

    def test_add(self, post_request, controller, user, use_cases_mock):
        (method, body) = post_request
        response = controller.handle(method, body)
        use_cases_mock.add.assert_called_once()
        assert response == (
            201,
            {
                "user": {
                    "pk": user.pk,
                    "name": user.name,
                    "email": user.email,
                }
            },
        )

    def test_add_wrong(self, post_request_wrong, controller, use_cases_mock):
        (method, body) = post_request_wrong
        response = controller.handle(method, body)
        use_cases_mock.add.assert_not_called()
        assert response == (
            400,
            {"message": "Bad request"},
        )

    def test_get(self, get_request, users, controller, use_cases_mock):
        (method, body) = get_request
        response = controller.handle(method, body)
        use_cases_mock.get.assert_called_once()
        user = users[0]
        assert response == (
            200,
            {
                "users": [
                    {
                        "pk": user.pk,
                        "name": user.name,
                        "email": user.email,
                    }
                ]
            },
        )

    def test_edit(self, put_request, controller, use_cases_mock):
        (method, body) = put_request
        response = controller.handle(method, body)
        use_cases_mock.edit.assert_called_once()
        assert response == (200, {})

    def test_edit_wrong(self, put_request_wrong, controller, use_cases_mock):
        (method, body) = put_request_wrong
        response = controller.handle(method, body)
        use_cases_mock.edit.assert_not_called()
        assert response == (
            400,
            {"message": "Bad request"},
        )

    def test_delete(self, delete_request, controller, use_cases_mock):
        (method, body) = delete_request
        response = controller.handle(method, body)
        use_cases_mock.delete.assert_called_once()
        assert response == (200, {})

    def test_delete_wrong(
        self, delete_request_wrong, controller, use_cases_mock
    ):
        (method, body) = delete_request_wrong
        response = controller.handle(method, body)
        use_cases_mock.delete.assert_not_called()
        assert response == (
            400,
            {"message": "Bad request"},
        )

    def test_invalid_method(self, invalid_method_request, controller):
        (method, body) = invalid_method_request
        response = controller.handle(method, body)
        assert response == (
            405,
            {"message": "Method not allowed"},
        )
