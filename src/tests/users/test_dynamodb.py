import pytest
from users.dynamodb import DynamoDBUseCases
from users.models import User
from users.protocols import UserDTO


class TestDynamoDBUseCases:
    @pytest.fixture(autouse=True)
    def dynamo_mock(self, mocker):
        return mocker.patch("boto3.resource")

    @pytest.fixture
    def dynamo_uc(self):
        return DynamoDBUseCases()

    @pytest.fixture
    def dynamo_table_mock(self, dynamo_uc, user_dict, user):
        mock = dynamo_uc._table
        mock.add.return_value = user
        mock.scan.return_value = {"Items": [user_dict]}
        return dynamo_uc._table

    @pytest.fixture
    def user_dto(self):
        return UserDTO(pk=1, name="Gaba", email="gaba@gaba.com")

    @pytest.fixture
    def user_dict(self, user_dto):
        return {
            "pk": user_dto.pk,
            "name": user_dto.name,
            "email": user_dto.email,
        }

    @pytest.fixture
    def sample_pk(self):
        return 1

    @pytest.fixture
    def user(self, sample_pk):
        return User(pk=sample_pk, name="Gaba", email="gaba@gaba.com")

    def test_add(self, dynamo_uc, dynamo_table_mock, user_dto):
        user = dynamo_uc.add(user_dto)
        dynamo_table_mock.put_item.assert_called_once_with(
            Item={
                "pk": user.pk,
                "name": user_dto.name,
                "email": user_dto.email,
            }
        )
        assert all(
            [
                user.name == user_dto.name,
                user.email == user_dto.email,
            ]
        )

    def test_edit(self, dynamo_uc, dynamo_table_mock, user_dto):
        dynamo_uc.edit(user_dto)
        dynamo_table_mock.update_item.assert_called_once_with(
            Key={"pk": user_dto.pk},
            UpdateExpression="set #name=:n, #email=:e",
            ExpressionAttributeValues={
                ":n": user_dto.name,
                ":e": user_dto.email,
            },
            ExpressionAttributeNames={"#name": "name", "#email": "email"},
        )

    def test_get(self, dynamo_uc, dynamo_table_mock, user_dict):
        users = dynamo_uc.get()
        dynamo_table_mock.scan.assert_called_once_with()
        user = users[0]
        assert all(
            [
                user.pk == user_dict["pk"],
                user.name == user_dict["name"],
                user.email == user_dict["email"],
            ]
        )

    def test_delete(self, dynamo_uc, dynamo_table_mock, sample_pk):
        dynamo_uc.delete(sample_pk)
        dynamo_table_mock.delete_item.assert_called_once_with(
            Key={"pk": sample_pk},
        )
