import pytest
from users.models import User


class TestModels:

    SAMPLE_PK = "1"

    @pytest.fixture
    def user(self):
        return User(
            pk=self.SAMPLE_PK,
            name="Ronaldinho Gaúcho",
            email="ronaldinho@bruxo.com",
        )

    def test_to_dict(self, user):
        result = user.to_dict()
        assert result == {
            "pk": user.pk,
            "name": user.name,
            "email": user.email,
        }
